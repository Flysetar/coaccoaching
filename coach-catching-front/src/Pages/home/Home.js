import React from "react";
import NavbarLinks from "../../Components/navbarlinks/NavbarLinks"
import Logo from "../../Components/logo/Logo"
import Discover from "../../Components/discover/Discover"


import './Home.css'
class Home extends React.Component {
  Constructor(props) {
  }

  render() {
    return(
      <div>
        <div className="colored-bg"></div>
        <NavbarLinks/>
        <Logo/>
        <Discover/>
      </div>
    );
  
  }
}

export default Home;
