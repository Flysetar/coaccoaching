import React from "react";
import Home from "./Pages/home/Home";
import Connect from "./Pages/connect/Connect";
import Register from "./Pages/register/Register";


import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import "./App.css";
class App extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Router>
          <Routes>
            <Route path="/" element={<Home />}/>     
            <Route path="/connect" element={<div><Home /><Connect /></div>}/>        
            <Route path="/register" element={<div><Home /><Register /></div>}/>  
          </Routes>
      </Router>
    );
  }
}

export default App;
