import React from "react";
import ConnectForm from "../connectform/ConnectForm"
import { Link } from "react-router-dom";


import "./ConnectHome.css";
class ConnectHome extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isConnectWithMail : false}
    this.ShowMailConnectDialog = this.ShowMailConnectDialog.bind(this);
  }

  componentDidMount() {
  }

  ShowMailConnectDialog(){
    this.setState({isConnectWithMail : true});
  }

  

  render() {
    if(this.state.isConnectWithMail == true){
      return(
        <ConnectForm/>
      )
    }else{
      return (
        <div className="bg-ctnr">
          <div className="connect-ctnr">
            <div className="close-form">
            <Link to="/" >x</Link>
            </div>
            <button className="connect-apple">
              <p>Se connecter avec Apple</p>
            </button>
            <button className="connect-google">
              <p>Se connecter avec Google</p>
            </button>
  
            <button className="connect-facebook">
              <p>Se connecter avec Facebook</p>
            </button>
            <h1>Ou</h1>
            <button className="connect-mail" onClick={this.ShowMailConnectDialog}>
              <p>Se connecter avec un Mail</p>
            </button>
            <br/>
            <br/>
            <br/>
            <p>En créant un compte, vous acceptez les termes d'utilisations.</p>
            <br/>
          </div>
        </div>
      );
    }
    
  }
}

export default ConnectHome;
