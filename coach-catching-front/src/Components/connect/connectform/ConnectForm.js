import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import "./ConnectForm.css";



const url = "http://localhost:4000/";

class ConnectMail extends React.Component {

  constructor(props) {
    super(props);
    this.login = this.login.bind(this);
  }

  login() {
    let email = document.getElementById('mail').value;
    let password = document.getElementById('password').value;

    let data = {
      email: email,
      password: password
    }
    axios({
      method: 'post',
      url: `${url}api/v1/login`,
      data: data
    })
      .then(function (response) {
          console.log(response)
      });
  }


  
  


  render() {
    return (
      <div className="bg-ctnr">
          <div className="connect-ctnr">
            <div className="close-form">
              <Link to="/" >x</Link>
            </div>
            <div className="label"> Addresse mail :</div>
            <input name="email" id="mail" type="mail" className="input "/>
            <div className="label"> Mot de passe :</div>
            <input name="password" id="password" type="password" className="input"/>
            
            <button className="connect-mail" onClick={this.login}>
              <p>Se connecter avec un Mail</p>
            </button>
            <br/>
            <br/>
            <br/>
            <p>En créant un compte, vous acceptez les termes d'utilisations.</p>
            <br/>
          </div>
        </div>
    );
  }
}

export default ConnectMail;
