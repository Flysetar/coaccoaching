import React from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import "./RegisterForm.css";

const url = "http://localhost:4000/";

class RegisterForm extends React.Component {
  constructor(props) {
    super(props);
    this.register = this.register.bind(this);
  }

  register(event) {
    let firstName = document.getElementById("password").value;
    let password = document.getElementById("password").value;
    let email = document.getElementById("mail").value;
    let lastName = document.getElementById("password").value;
    let data = {
      firstname: firstName,
      lastname: lastName,
      email: email,
      password: password,
      isCoach: false,
    };
    const headers = {
      "Access-Control-Allow-Origin": "*",
    };
    axios({
      method: "POST",
      url: `${url}api/v1/register`,
      data: data,
      headers: headers,
    }).then(function (response) {
      console.log(response);
    });
  }

  render() {
    return (
      <div className="bg-ctnr">
        <div className="connect-ctnr">
          <div className="close-form">
            <Link to="/">x</Link>
          </div>
          <div className="label"> First Name :</div>
          <input name="firstname" id="fname" type="text" className="input " />
          <div className="label"> Last Name :</div>
          <input name="lastname" id="lname" type="text" className="input " />
          <div className="label"> Addresse mail :</div>
          <input name="email" id="mail" type="mail" className="input " />
          <div className="label"> Mot de passe :</div>
          <input
            name="password"
            id="password"
            type="password"
            className="input"
          />

          <button className="connect-mail" onClick={this.register}>
            <p>S'inscrire</p>
          </button>
          <br />
          <p>En créant un compte, vous acceptez les termes d'utilisations.</p>
          <br />
        </div>
      </div>
    );
  }
}

export default RegisterForm;
