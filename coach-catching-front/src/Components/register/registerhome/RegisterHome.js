import React from "react";
import { Link } from "react-router-dom";
import RegisterForm from "../registerform/RegisterForm"

import "./RegisterHome.css";
class RegisterHome extends React.Component {

  constructor(props) {
    super(props);
    this.state = {isRegisterWithMail : false}
    this.ShowMailRegisterDialog = this.ShowMailRegisterDialog.bind(this);
  }

  componentDidMount() {
  }

  ShowMailRegisterDialog(){
    this.setState({isRegisterWithMail : true});
  }

  

  render() {
    if(this.state.isRegisterWithMail == true){
      return(
        <RegisterForm/>
      )
    }else{
      return (
        <div className="bg-ctnr">
          <div className="connect-ctnr">
            <div className="close-form">
            <Link to="/" >x</Link>
            </div>
            <button className="connect-apple">
              <p>S'inscrire avec Apple</p>
            </button>
            <button className="connect-google">
              <p>S'inscrire avec Google</p>
            </button>
  
            <button className="connect-facebook">
              <p>S'inscrire avec Facebook</p>
            </button>
            <h1>Ou</h1>
            <button className="connect-mail" onClick={this.ShowMailRegisterDialog}>
              <p>S'inscrire avec un Mail</p>
            </button>
            <br/>
            <br/>
            <br/>
            <p>En créant un compte, vous acceptez les termes d'utilisations.</p>
            <br/>
          </div>
        </div>
      );
    }
    
  }
}

export default RegisterHome;
