import React from "react";

import { Link } from 'react-router-dom'

import "./NavbarLinks.css";
class NavbarLinks extends React.Component {
  Constructor(props) {}

  render() {
    return (
      <div className="link-ctnr">
        <div class="connect-btn" type="button">   
          <Link to="/register">S'inscrire</Link>
        </div>
        <div class="register-btn" type="button" onclick="">
          <Link to="/connect">J'ai déjà un compte</Link>
        </div>
      </div>
    );
  }
}

export default NavbarLinks;
