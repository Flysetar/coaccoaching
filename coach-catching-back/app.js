const express = require('express');
const cookieParser = require('cookie-parser');
const fileUpload = require('express-fileupload');
const app = express();
const path = require('path');
const errorMiddleware = require('./middlewares/errors')
let cors = require('cors')

require('dotenv').config({ path: 'coach-catching-back/config/config.env' })

app.use(cors({ origin: 'http://localhost:3000' }));
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(cookieParser())
app.use(fileUpload())

const auth = require('./routes/auth');
const category = require('./routes/category');
const course = require('./routes/course');
const coach = require('./routes/coach');

app.use('/api/v1', auth)
app.use('/api/v1', category)
app.use('/api/v1', course)
app.use('/api/v1', coach)

if (process.env.NODE_ENV === 'PRODUCTION') {
    app.use(express.static(path.join(__dirname, './build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, './build/index.html'))
    })
}

app.use(errorMiddleware)

module.exports = app;