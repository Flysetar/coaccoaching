const app = require('./app');
const connectDatabase = require('./config/database');

// Handle uncaught exceptions
process.on('uncaughtException', err => {
    console.log(`ERROR: ${err.message}`);
    console.log('Shutting down due to uncaught exception');
    process.exit(1);
})

if (process.env.NODE_ENV !== 'PRODUCTION') {
    require('dotenv').config({ path: 'config/config.env' })
}

connectDatabase();

const server = app.listen(process.env.PORT, () => {
    console.log(`Server started on PORT : ${process.env.PORT} in ${process.env.NODE_ENV} mode.`)
})

// Handle unhandled promise rejection
process.on('unhandledRejection', err => {
    console.log(`ERROR: ${err.message}`);
    console.log('Shutting down server during unhandled promise rejection');
    server.close(() => {
        process.exit(1);
    })
})