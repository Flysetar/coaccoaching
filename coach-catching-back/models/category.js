const mongoose = require('mongoose');

const categorySchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Veuillez entrer le nom de la catégorie'],
        trim: true,
        maxLength: [100, 'Le nom de la catégorie ne peut pas excéder 100 caractères']
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})

module.exports = mongoose.model('Category', categorySchema);