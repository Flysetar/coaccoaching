const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const crypto = require('crypto');

const userSchema = new mongoose.Schema({
    firstname: {
        type: 'String',
        required: [true, 'Entrez votre prénom'],
        maxlength: [30, 'Votre nom ne peut pas excéder 30 caractères']
    },
    lastname: {
        type: 'String',
        required: [true, 'Entrez votre nom'],
        maxlength: [30, 'Votre nom ne peut pas excéder 30 caractères']
    },
    email: {
        type: 'String',
        required: [true, 'Entrez votre e-mail'],
        unique: true,
        validate: [validator.isEmail, 'Entrez un e-mail valide']
    },
    password: {
        type: 'String',
        required: [true, 'Entrez votre mot de passe'],
        minlength: [6, 'Votre mot de passe doit faire au minimum 6 caractères'],
        select: false
    },
    role: {
        type: 'String',
        default: 'user'
    },
    isCoach: {
        type: 'Boolean',
    },
    createdAt: {
        type: Date,
        default: Date.now
    },
    resetPasswordToken: String,
    resetPasswordExpire: Date
})

// Encrypting password before saving user
userSchema.pre('save', async function(next) {
    if(!this.isModified('password')) {
        next()
    }

    this.password = await bcrypt.hash(this.password, 10)
})

// Compare user password
userSchema.methods.comparePassword = async function (enteredPassword) {
    return await bcrypt.compare(enteredPassword, this.password)
}

// Return JWT
userSchema.methods.getJwtToken = function() {
    return jwt.sign({ id: this._id }, process.env.JWT_SECRET, {
        expiresIn: process.env.JWT_EXPIRES_TIME
    })
}

// Generate password rest token
userSchema.methods.getResetPasswordToken = function() {
    const resetToken = crypto.randomBytes(20).toString('hex');

    // Hash and set to resetPasswordToken
    this.resetPasswordToken = crypto.createHash('sha256').update(resetToken).digest('hex');
    
    // Set token expired time
    this.resetPasswordExpire = Date.now() + 30 * 60 * 1000;
    
    return resetToken;
}

module.exports = mongoose.model('User', userSchema);