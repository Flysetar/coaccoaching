const Course = require("../models/course");
const ErrorHandler = require("../utils/errorHandler");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");


// Post course => /api/v1/admin/course/new
exports.newCourse = catchAsyncErrors(async (req, res, next) => {
    const course = await Course.create(req.body);
    res.status(201).json({
        success: true,
        course,
    });
});

// Get all courses => /api/v1/admin/courses/
exports.getCourses = catchAsyncErrors(async (req, res, next) => {
    const courses = await Course.find()
    const coursesCount = await Course.countDocuments();

    res.status(200).json({
        success: true,
        coursesCount,
        courses
    })
})

// Get single course details => /api/v1/course/:id
exports.getSingleCourse = catchAsyncErrors(async (req, res, next) => {
    const course = await Course.findById(req.params.id);

    if (!course) {
        return next(new ErrorHandler("Course not found", 404));
    }

    res.status(200).json({
        success: true,
        course,
    });
});

// Update course => /api/v1/admin/category/:id
exports.updateCourse = catchAsyncErrors(async (req, res, next) => {
    let course = await Course.findById(req.params.id);

    if (!course) {
        return next(new ErrorHandler("Course not found", 404));
    }

    course = await Course.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });

    res.status(200).json({
        success: true,
        course,
    });
});

// Delete course => /api/v1/admin/course/:id
exports.deleteCourse = catchAsyncErrors(async (req, res, next) => {
    const course = await Course.findById(req.params.id);

    if (!course) {
        return next(new ErrorHandler("Course not found", 404));
    }

    await course.deleteOne();

    res.status(200).json({
        success: true,
        message: "Course deleted successfully",
    });
});