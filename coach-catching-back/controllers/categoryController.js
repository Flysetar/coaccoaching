const Category = require("../models/category");
const ErrorHandler = require("../utils/errorHandler");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");


// Post category => /api/v1/admin/category/new
exports.newCategory = catchAsyncErrors(async (req, res, next) => {
    const category = await Category.create(req.body);
    res.status(201).json({
        success: true,
        category,
    });
});

// Get all categories => /api/v1/admin/categories/
exports.getCategories = catchAsyncErrors(async (req, res, next) => {
    const categories = await Category.find()
    const categoriesCount = await Category.countDocuments();

    res.status(200).json({
        success: true,
        categoriesCount,
        categories
    })
})

// Get single category details => /api/v1/category/:id
exports.getSingleCategory = catchAsyncErrors(async (req, res, next) => {
    const category = await Category.findById(req.params.id);
  
    if (!category) {
      return next(new ErrorHandler("Category not found", 404));
    }
  
    res.status(200).json({
      success: true,
      category,
    });
  });

// Update category => /api/v1/admin/category/:id
exports.updateCategory = catchAsyncErrors(async (req, res, next) => {
    let category = await Category.findById(req.params.id);

    if (!category) {
        return next(new ErrorHandler("Category not found", 404));
    }

    category = await Category.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });

    res.status(200).json({
        success: true,
        category,
    });
});

// Delete category => /api/v1/admin/category/:id
exports.deleteCategory = catchAsyncErrors(async (req, res, next) => {
    const category = await Category.findById(req.params.id);

    if (!category) {
        return next(new ErrorHandler("Category not found", 404));
    }

    await category.deleteOne();

    res.status(200).json({
        success: true,
        message: "Category deleted successfully",
    });
});