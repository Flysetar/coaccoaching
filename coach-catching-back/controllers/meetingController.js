const Meeting = require("../models/meeting");
const ErrorHandler = require("../utils/errorHandler");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");


// Post meeting => /api/v1/admin/meeting/new
exports.newMeeting = catchAsyncErrors(async (req, res, next) => {
    const meeting = await Meeting.create(req.body);
    res.status(201).json({
        success: true,
        meeting,
    });
});

// Get all meetings => /api/v1/admin/meetings/
exports.getMeetings = catchAsyncErrors(async (req, res, next) => {
    const meetings = await Meeting.find()
    const meetingsCount = await Meeting.countDocuments();

    res.status(200).json({
        success: true,
        meetingsCount,
        meetings
    })
})

// Get single meeting details => /api/v1/meeting/:id
exports.getSingleMeeting = catchAsyncErrors(async (req, res, next) => {
    const meeting = await Meeting.findById(req.params.id);

    if (!meeting) {
        return next(new ErrorHandler("Meeting not found", 404));
    }

    res.status(200).json({
        success: true,
        meeting,
    });
});

// Update meeting => /api/v1/admin/meeting/:id
exports.updateMeeting = catchAsyncErrors(async (req, res, next) => {
    let meeting = await Meeting.findById(req.params.id);

    if (!meeting) {
        return next(new ErrorHandler("Meeting not found", 404));
    }

    meeting = await Meeting.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true,
    });

    res.status(200).json({
        success: true,
        meeting
    });
});

// Delete meeting => /api/v1/admin/meeting/:id
exports.deleteMeeting = catchAsyncErrors(async (req, res, next) => {
    const meeting = await Meeting.findById(req.params.id);

    if (!meeting) {
        return next(new ErrorHandler("Meeting not found", 404));
    }

    await meeting.deleteOne();

    res.status(200).json({
        success: true,
        message: "Meeting deleted successfully",
    });
});

const 