const User = require("../models/user");
const ErrorHandler = require("../utils/errorHandler");
const catchAsyncErrors = require("../middlewares/catchAsyncErrors");

// Get all coaches => /api/v1/admin/coaches/
exports.getCoaches = catchAsyncErrors(async (req, res, next) => {
    const coaches = await User.find({ 'isCoach': true })
    const coachesCount = await User.countDocuments({ 'isCoach': true });

    res.status(200).json({
        success: true,
        coachesCount,
        coaches
    })
})

// Get single coach details => /api/v1/coach/:id
exports.getSingleCoach = catchAsyncErrors(async (req, res, next) => {
    const coach = await User.findById(req.params.id);

    if (!coach || coach.isCoach === false) {
        return next(new ErrorHandler("The user you're searching doesn't exist or isn't a coach", 404));
    }

    res.status(200).json({
        success: true,
        coach,
    });
});