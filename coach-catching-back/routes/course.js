const express = require("express");
const router = express.Router();

const { isAuthenticatedUser, authorizeRoles } = require("../middlewares/auth");

const  {
    newCourse,
    getCourses,
    getSingleCourse,
    updateCourse,
    deleteCourse
} = require("../controllers/courseController")

router.route("/courses").get(getCourses);

router.route("/course/:id").get(getSingleCourse);

router
  .route("/admin/course/new")
  .post(isAuthenticatedUser, authorizeRoles('admin'), newCourse);

router
  .route("/admin/course/:id")
  .put(isAuthenticatedUser, authorizeRoles('admin'), updateCourse)
  .delete(isAuthenticatedUser, authorizeRoles('admin'), deleteCourse);

module.exports = router;