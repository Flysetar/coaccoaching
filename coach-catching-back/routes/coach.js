const express = require("express");
const router = express.Router();

const  {
    getCoaches,
    getSingleCoach,
} = require("../controllers/coachController")

router.route("/coaches").get(getCoaches);

router.route("/coach/:id").get(getSingleCoach);

module.exports = router;